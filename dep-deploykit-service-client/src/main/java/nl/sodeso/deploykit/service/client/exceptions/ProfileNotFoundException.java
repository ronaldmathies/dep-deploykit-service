package nl.sodeso.deploykit.service.client.exceptions;

import nl.sodeso.commons.restful.model.Message;

/**
 * @author Ronald Mathies
 */
public class ProfileNotFoundException extends Exception {

    public ProfileNotFoundException(Message message) {
        super(message.getCode() + " - " + message.getMessage());
    }

}
