package nl.sodeso.deploykit.service.client;

import nl.sodeso.deploykit.service.client.exceptions.ProfileNotFoundException;
import nl.sodeso.deploykit.service.client.exceptions.ProfileRetreivalException;
import nl.sodeso.commons.restful.model.*;
import nl.sodeso.deploykit.service.model.Profile;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * @author Ronald Mathies
 */
public class DeployKitServiceClient {

    private static final String CONTEXT = "/deploykit/profile";
    private static final String PARAM_LABEL = "label";
    private static final String PARAM_VERSION = "version";

    private String host = null;
    private int port;

    public DeployKitServiceClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public Profile findProfile(String label, String version) throws ProfileNotFoundException, ProfileRetreivalException {
        try {
            Client client = ClientBuilder.newBuilder().build();
            WebTarget webTarget = client.target(new URL("http", this.host, this.port, CONTEXT).toURI())
                    .queryParam(PARAM_LABEL, label)
                    .queryParam(PARAM_VERSION, version);

            Response response = webTarget.request().get();
            if (response.getStatus() == Response.Status.OK.getStatusCode()) {
                try {
                    return response.readEntity(Profile.class);
                } catch (ProcessingException e) {
                    throw new ProfileRetreivalException("Failed to interpret data received from DeployKit service.", e);
                }
            } else if (response.getStatus() == Response.Status.BAD_REQUEST.getStatusCode()) {
                try {
                    Message message = response.readEntity(Message.class);
                    throw new ProfileNotFoundException(message);
                } catch (ProcessingException e) {
                    throw new ProfileRetreivalException("Failed to interpret data received from DeployKit service.", e);
                }
            }

            throw new ProfileRetreivalException("Unexpected error occurred during retrieval of profile from DeployKit service, status code: " + response.getStatus() + " - " + response.getStatusInfo().getReasonPhrase());
        } catch (MalformedURLException e) {
            throw new ProfileRetreivalException("Failed to construct a valid URL from '" + this.host + ":" + this.port, e);
        } catch (URISyntaxException e) {
            throw new ProfileRetreivalException("Failed to construct a valid URI from '" + this.host + ":" + this.port, e);
        } catch (ProcessingException e ) {
            throw new ProfileRetreivalException("Failed to connect to the DeployKit Service '" + this.host + ":" + this.port, e);

        }
    }

    public void registerAsNewNode() {



    }

}
