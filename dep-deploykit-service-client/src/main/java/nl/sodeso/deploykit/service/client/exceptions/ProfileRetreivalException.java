package nl.sodeso.deploykit.service.client.exceptions;

/**
 * @author Ronald Mathies
 */
public class ProfileRetreivalException extends Exception {

    public ProfileRetreivalException(String message) {
        super(message);
    }

    public ProfileRetreivalException(String message, Throwable cause) {
        super(message, cause);
    }

}
