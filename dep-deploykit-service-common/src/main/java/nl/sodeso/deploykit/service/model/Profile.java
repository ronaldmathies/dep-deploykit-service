package nl.sodeso.deploykit.service.model;

import nl.sodeso.commons.token.ITokenResolver;
import nl.sodeso.deploykit.service.model.accesscontrol.AccessControl;
import nl.sodeso.deploykit.service.model.connectors.Http;
import nl.sodeso.deploykit.service.model.connectors.Https;
import nl.sodeso.deploykit.service.model.datasource.datasource.Datasource;
import nl.sodeso.deploykit.service.model.general.credential.Credential;
import nl.sodeso.deploykit.service.model.services.ldap.LdapService;
import nl.sodeso.deploykit.service.model.services.webservice.Webservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class Profile implements ITokenResolver {

    private Map<String, String> placeholders;

    private String uuid = null;
    private String label = null;
    private String version = null;

    private List<Datasource> datasources = null;
    private List<Credential> credentials = null;
    private List<LdapService> ldapServices = null;
    private List<Webservice> webservices = null;

    private Https https = null;
    private Http http = null;

    private AccessControl accessControl = null;

    public Profile() {}

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<Datasource> getDatasources() {
        return datasources;
    }

    public void setDatasources(List<Datasource> datasources) {
        this.datasources = datasources;
    }

    public List<Credential> getCredentials() {
        return this.credentials;
    }

    public void setCredentials(List<Credential> credentials) {
        this.credentials = credentials;
    }

    public Https getHttps() {
        return this.https;
    }

    public void setHttps(Https https) {
        this.https = https;
    }

    public Http getHttp() {
        return http;
    }

    public void setHttp(Http http) {
        this.http = http;
    }

    public List<LdapService> getLdapServices() {
        return ldapServices;
    }

    public void setLdapServices(List<LdapService> ldapServices) {
        this.ldapServices = ldapServices;
    }

    public AccessControl getAccessControl() {
        return accessControl;
    }

    public void setAccessControl(AccessControl accessControl) {
        this.accessControl = accessControl;
    }

    public List<Webservice> getWebservices() {
        return webservices;
    }

    public void setWebservices(List<Webservice> webservices) {
        this.webservices = webservices;
    }

    @Override
    public String resolveToken(String tokenName) {
        if (placeholders == null) {
            collectPlaceholders();
        }

        if (placeholders.containsKey(tokenName)) {
            return placeholders.get(tokenName);
        }

        for (Datasource datasource : datasources) {
            String value = datasource.resolveToken(tokenName);
            if (value != null) {
                return value;
            }
        }

        for (Credential credential : credentials) {
            String value = credential.resolveToken(tokenName);
            if (value != null) {
                return value;
            }
        }

        for (LdapService ldapService : ldapServices) {
            String value = ldapService.resolveToken(tokenName);
            if (value != null) {
                return value;
            }
        }

        for (Webservice webservice : webservices) {
            String value = webservice.resolveToken(tokenName);
            if (value != null) {
                return value;
            }
        }

        try {
            String value = accessControl.resolveToken(tokenName);
            if (value != null) {
                return value;
            }
        } catch (IOException e) {
            // No token found.
        }

        return null;
    }

    private void collectPlaceholders() {
        placeholders = new HashMap<>();
        placeholders.put("profile.name", getLabel());
        placeholders.put("profile.version", getVersion());
    }

    public List<Resource> allResources() {
        List<Resource> resources = new ArrayList<>();
        resources.addAll(this.datasources);
        resources.addAll(this.credentials);
        resources.addAll(this.ldapServices);
        resources.add(this.https);
        resources.add(this.http);
        resources.add(this.accessControl);
        resources.addAll(this.webservices);

        return resources;
    }

    public Resource findResourceByUuid(String uuid) {
        return findResourceByUuid(uuid, null);
    }

    public Resource findResourceByUuid(String uuid, Class clazz) {
        for (Resource resource : allResources()) {
            if (clazz == null || resource.getClass().equals(clazz)) {
                if (resource.getUuid().equalsIgnoreCase(uuid)) {
                    return resource;
                }
            }
        }

        return null;
    }
}
