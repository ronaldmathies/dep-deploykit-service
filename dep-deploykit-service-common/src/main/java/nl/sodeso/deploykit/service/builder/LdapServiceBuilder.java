package nl.sodeso.deploykit.service.builder;

import nl.sodeso.deploykit.console.domain.services.ldap.entities.DoLdapService;
import nl.sodeso.deploykit.service.model.services.ldap.LdapService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class LdapServiceBuilder {

    public static List<LdapService> from(List<DoLdapService> doLdapServices) {
        List<LdapService> ldapServices = new ArrayList<>();

        for (DoLdapService doLdapService : doLdapServices) {
            ldapServices.add(from(doLdapService));
        }

        return ldapServices;
    }

    public static LdapService from(DoLdapService doLdapService) {
        LdapService ldapService = new LdapService();

        ldapService.setUuid(doLdapService.getUuid());
        ldapService.setPlaceholderPrefix(doLdapService.getPlaceholderPrefix().getValue());
        ldapService.setDomain(doLdapService.getDomain().getValue());
        ldapService.setPort(doLdapService.getPort().getValue());
        ldapService.setUsername(doLdapService.getCredential().getUsername().getValue());
        ldapService.setPassword(doLdapService.getCredential().getPassword().getValue());

        return ldapService;
    }


}
