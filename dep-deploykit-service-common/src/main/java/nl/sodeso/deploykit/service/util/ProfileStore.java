package nl.sodeso.deploykit.service.util;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.sodeso.deploykit.service.model.Profile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ProfileStore {

    private static ObjectMapper objectMapper = new ObjectMapper();

    public static void write(File profileFolder, Profile profile) throws FailedToWriteProfileException {
        try {
            File profileFile = new File(profileFolder, profile.getUuid() + ".json");
            if (profileFile.exists()) {
                if (!profileFile.delete()) {
                    throw new FailedToWriteProfileException("Failed to delete existing profile.");
                }
            }

            objectMapper.writeValue(profileFile, profile);
        } catch (JsonGenerationException e) {
            throw new FailedToWriteProfileException("Failed to generate correctly structured JSON format.", e);
        } catch (JsonMappingException e) {
            throw new FailedToWriteProfileException("Serialization exception occurred while writing the profile.", e);
        } catch (IOException e) {
            throw new FailedToWriteProfileException("Failed to store profile.", e);
        }
    }

    public static Profile read(File profileFolder, String uuid) throws FailedToReadProfileException {
        try {
            File profileFile = new File(profileFolder, uuid + ".json");
            if (profileFile.exists()) {
                return objectMapper.readValue(profileFile, Profile.class);
            }

            return null;
        } catch (JsonParseException e) {
            throw new FailedToReadProfileException("Failed to read the profile, incorrectly structured JSON.", e);
        } catch (JsonMappingException e) {
            throw new FailedToReadProfileException("Serialization exception occurred while reading the profile.", e);
        } catch (IOException e) {
            throw new FailedToReadProfileException("Failed to read profile.", e);
        }
    }

    public static List<Profile> getProfilesSharingResource(File profileFolder, String resourceUuid) throws FailedToReadProfileException {
        List<Profile> profiles = new ArrayList<>();
        for (String profileUuid : allProfileUuids(profileFolder)) {
            Profile profile = read(profileFolder, profileUuid);
            if (profile.findResourceByUuid(resourceUuid) != null) {
                profiles.add(profile);
            }
        }

        return profiles;
    }

    public static boolean isResourceInUseByOtherProfile(File profileFolder, String profileToExcludeUuid, String resourceUuid) throws FailedToReadProfileException {
        for (String profileUuid : allProfileUuids(profileFolder)) {

            // Skip the profile we want to ignore.
            if (profileUuid.equalsIgnoreCase(profileToExcludeUuid)) {
                continue;
            }

            Profile profile = read(profileFolder, profileUuid);
            if (profile.findResourceByUuid(resourceUuid) != null) {
                return true;
            }
        }

        return false;
    }

    public static List<String> allProfileUuids(File profileFolder) {
        if (profileFolder.exists()) {
            List<String> jsonFiles = Arrays.asList(
                    profileFolder.list((dir, name) -> name.endsWith(".json"))
            );


            List<String> uuids = new ArrayList<>();
            for (String jsonFile : jsonFiles) {
                uuids.add(jsonFile.substring(0, jsonFile.length() - 5));
            }

            return uuids;
        }

        return new ArrayList<String>();
    }

}
