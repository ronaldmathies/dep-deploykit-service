package nl.sodeso.deploykit.service.model.services.ldap;

import nl.sodeso.commons.token.ITokenResolver;
import nl.sodeso.deploykit.console.client.application.services.ldap.LdapServicePlaceHolderSuffix;
import nl.sodeso.deploykit.service.model.Resource;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class LdapService extends Resource implements ITokenResolver {

    private Map<String, String> placeholders = null;

    private String placeholderPrefix = null;

    private String domain;
    private int port;
    private String username;
    private String password;

    public LdapService() {}

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPlaceholderPrefix() {
        return placeholderPrefix;
    }

    public void setPlaceholderPrefix(String placeholderPrefix) {
        this.placeholderPrefix = placeholderPrefix;
    }

    @Override
    public String resolveToken(String tokenName) {
        if (placeholders == null) {
            collectPlaceholders();
        }

        if (placeholders.containsKey(tokenName)) {
            return placeholders.get(tokenName);
        }

        return null;
    }

    public void collectPlaceholders() {
        placeholders = new HashMap<>();
        placeholders.put(LdapServicePlaceHolderSuffix.LDAP_DOMAIN(getPlaceholderPrefix()), getDomain());
        placeholders.put(LdapServicePlaceHolderSuffix.LDAP_PORT(getPlaceholderPrefix()), getDomain());
        placeholders.put(LdapServicePlaceHolderSuffix.LDAP_USERNAME(getPlaceholderPrefix()), getUsername());
        placeholders.put(LdapServicePlaceHolderSuffix.LDAP_PASSWORD(getPlaceholderPrefix()), getPassword());
    }
}
