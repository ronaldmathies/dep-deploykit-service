package nl.sodeso.deploykit.service.builder;

import nl.sodeso.deploykit.console.domain.connectors.http.entities.DoHttp;
import nl.sodeso.deploykit.service.model.connectors.Http;

/**
 * @author Ronald Mathies
 */
public class HttpBuilder {

    public static Http from(DoHttp doHttp) {
        if (doHttp != null) {

            Http http = new Http();
            http.setUuid(doHttp.getUuid());
            http.setLabel(doHttp.getLabel().getValue());
            http.setPort(doHttp.getPort().getValue());
            http.setTimeout(doHttp.getTimeout().getValue());
            http.setHost(doHttp.getHost().getValue());

            return http;
        }

        return null;
    }

}
