package nl.sodeso.deploykit.service.builder;

import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPool;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPoolJBoss;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPoolJetty;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoConnectionProperty;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.entities.DoJdbcDriver;
import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.entities.DoJdbcDriverJar;
import nl.sodeso.deploykit.service.builder.util.Base64Util;
import nl.sodeso.deploykit.service.model.datasource.datasource.ConnectionProperty;
import nl.sodeso.deploykit.service.model.datasource.datasource.Datasource;
import nl.sodeso.deploykit.service.model.datasource.jdbcdriver.JdbcDriver;
import nl.sodeso.deploykit.service.model.datasource.jdbcdriver.JdbcDriverJar;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class DatasourceBuilder {

    public static List<Datasource> from(List<DoDatasource> doDatasources) {
        List<Datasource> datasources = new ArrayList<>();

        for (DoDatasource doDatasource : doDatasources) {
            datasources.add(from(doDatasource));
        }

        return datasources;
    }

    public static Datasource from(DoDatasource doDatasource) {
        Datasource datasource = new Datasource();

        datasource.setUuid(doDatasource.getUuid());
        datasource.setLabel(doDatasource.getLabel().getValue());
        datasource.setPlaceholderPrefix(doDatasource.getPlaceholderPrefix().getValue());
        datasource.setJndi(doDatasource.getJndi().getValue());
        datasource.setUrl(doDatasource.getUrl().getValue());
        datasource.setUsername(doDatasource.getCredential().getUsername().getValue());
        datasource.setPassword(doDatasource.getCredential().getPassword().getValue());

        for (DoConnectionPool doConnectionPool : doDatasource.getConnectionPools()) {
            if (doConnectionPool instanceof DoConnectionPoolJBoss) {
                datasource.setJBossConnectionPool(ConnectionPoolBuilder.from((DoConnectionPoolJBoss) doConnectionPool));
            } else if (doConnectionPool instanceof DoConnectionPoolJetty) {
                datasource.setJettyConnectionPool(ConnectionPoolBuilder.from((DoConnectionPoolJetty) doConnectionPool));
            }
        }

        DoJdbcDriver doJdbcDriver = doDatasource.getJdbcDriver();
        JdbcDriver jdbcDriver = new JdbcDriver();
        jdbcDriver.setUuid(doJdbcDriver.getUuid());
        jdbcDriver.setLabel(doJdbcDriver.getLabel().getValue());
        for (DoJdbcDriverJar doJdbcDriverJar : doJdbcDriver.getJdbcDriverJars()) {
            JdbcDriverJar jdbcDriverJar = new JdbcDriverJar();
            jdbcDriverJar.setFilename(doJdbcDriverJar.getFilename());
            jdbcDriverJar.setJar(Base64Util.to(doJdbcDriverJar.getJar()).toString());
            jdbcDriver.getJdbcDriverJars().add(jdbcDriverJar);
        }

        datasource.setJdbcDriver(jdbcDriver);
        jdbcDriver.setJdbcDriverClass(doJdbcDriver.getJdbcDriverClass().getClassname());

        for (DoConnectionProperty doConnectionProperty : doDatasource.getProperties()) {
            ConnectionProperty connectionProperty = new ConnectionProperty();
            connectionProperty.setKey(doConnectionProperty.getKey().getValue());
            connectionProperty.setValue(doConnectionProperty.getValue().getValue());
            datasource.getConnectionProperties().add(connectionProperty);
        }

        return datasource;
    }

}
