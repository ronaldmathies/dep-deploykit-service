package nl.sodeso.deploykit.service.model.datasource.jdbcdriver;

import nl.sodeso.deploykit.service.model.Resource;

import java.util.ArrayList;
import java.util.Objects;

/**
 * @author Ronald Mathies
 */
public class JdbcDriver extends Resource {

    private String label;
    private String jdbcDriverClass;
    private ArrayList<JdbcDriverJar> jdbcDriverJars = new ArrayList<>();

    public JdbcDriver() {}

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getJdbcDriverClass() {
        return jdbcDriverClass;
    }

    public void setJdbcDriverClass(String jdbcDriverClass) {
        this.jdbcDriverClass = jdbcDriverClass;
    }

    public ArrayList<JdbcDriverJar> getJdbcDriverJars() {
        return this.jdbcDriverJars;
    }

    public void collectPlaceholders() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JdbcDriver that = (JdbcDriver) o;
        return Objects.equals(label, that.label);
    }

    @Override
    public int hashCode() {
        return Objects.hash(label);
    }
}
