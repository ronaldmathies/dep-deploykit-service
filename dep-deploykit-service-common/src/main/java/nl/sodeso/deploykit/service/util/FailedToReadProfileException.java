package nl.sodeso.deploykit.service.util;

/**
 * @author Ronald Mathies
 */
public class FailedToReadProfileException extends Exception {

    public FailedToReadProfileException(String message) {
        super(message);
    }

    public FailedToReadProfileException(String message, Throwable cause) {
        super(message, cause);
    }

    public FailedToReadProfileException(Throwable cause) {
        super(cause);
    }

    public FailedToReadProfileException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
