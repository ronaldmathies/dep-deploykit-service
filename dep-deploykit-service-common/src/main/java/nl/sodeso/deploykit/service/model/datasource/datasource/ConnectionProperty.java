package nl.sodeso.deploykit.service.model.datasource.datasource;

/**
 * @author Ronald Mathies
 */
public class ConnectionProperty {

    private String key;
    private String value;

    public ConnectionProperty() {}

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return this.key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

}
