package nl.sodeso.deploykit.service.util;

/**
 * @author Ronald Mathies
 */
public class FailedToWriteProfileException extends Exception {

    public FailedToWriteProfileException(String message) {
        super(message);
    }

    public FailedToWriteProfileException(String message, Throwable cause) {
        super(message, cause);
    }

    public FailedToWriteProfileException(Throwable cause) {
        super(cause);
    }

    public FailedToWriteProfileException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
