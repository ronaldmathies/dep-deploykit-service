package nl.sodeso.deploykit.service.model.datasource.connectionpool;

/**
 * @author Ronald Mathies
 */
public class JBossConnectionPool extends ConnectionPool {

    private Integer minPoolSize;
    private Integer maxPoolSize;
    private Integer initialPoolSize;
    private Boolean prefill;
    private String validationQuery= null;
    private String connectionInitSqls= null;
    private String defaultTransactionIsolation = null;

    private Integer blockingTimeoutMillis;
    private Integer idleTimeoutMinutes;
    private Integer allocationRetry;
    private Integer allocationRetryWaitMillis;

    public JBossConnectionPool() {}

    public Integer getMinPoolSize() {
        return minPoolSize;
    }

    public void setMinPoolSize(Integer minPoolSize) {
        this.minPoolSize = minPoolSize;
    }

    public Integer getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(Integer maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public Integer getInitialPoolSize() {
        return initialPoolSize;
    }

    public void setInitialPoolSize(Integer initialPoolSize) {
        this.initialPoolSize = initialPoolSize;
    }

    public Boolean getPrefill() {
        return prefill;
    }

    public void setPrefill(Boolean prefill) {
        this.prefill = prefill;
    }

    public Integer getBlockingTimeoutMillis() {
        return blockingTimeoutMillis;
    }

    public void setBlockingTimeoutMillis(Integer blockingTimeoutMillis) {
        this.blockingTimeoutMillis = blockingTimeoutMillis;
    }

    public Integer getIdleTimeoutMinutes() {
        return idleTimeoutMinutes;
    }

    public void setIdleTimeoutMinutes(Integer idleTimeoutMinutes) {
        this.idleTimeoutMinutes = idleTimeoutMinutes;
    }

    public Integer getAllocationRetry() {
        return allocationRetry;
    }

    public void setAllocationRetry(Integer allocationRetry) {
        this.allocationRetry = allocationRetry;
    }

    public Integer getAllocationRetryWaitMillis() {
        return allocationRetryWaitMillis;
    }

    public void setAllocationRetryWaitMillis(Integer allocationRetryWaitMillis) {
        this.allocationRetryWaitMillis = allocationRetryWaitMillis;
    }

    public String getValidationQuery() {
        return validationQuery;
    }

    public void setValidationQuery(String validationQuery) {
        this.validationQuery = validationQuery;
    }

    public String getConnectionInitSqls() {
        return connectionInitSqls;
    }

    public void setConnectionInitSqls(String connectionInitSqls) {
        this.connectionInitSqls = connectionInitSqls;
    }

    public String getDefaultTransactionIsolation() {
        return defaultTransactionIsolation;
    }

    public void setDefaultTransactionIsolation(String defaultTransactionIsolation) {
        this.defaultTransactionIsolation = defaultTransactionIsolation;
    }

}
