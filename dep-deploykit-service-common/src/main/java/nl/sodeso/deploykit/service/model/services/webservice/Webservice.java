package nl.sodeso.deploykit.service.model.services.webservice;

import nl.sodeso.commons.token.ITokenResolver;
import nl.sodeso.deploykit.console.client.application.services.webservice.WebservicePlaceHolderSuffix;
import nl.sodeso.deploykit.service.model.Resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class Webservice extends Resource implements ITokenResolver {

    private Map<String, String> placeholders = null;

    private String uuid;

    private String label;

    private String domain;
    private String context;
    private String path;
    private Integer port;
    private Integer timeout;

    private Boolean useWsa = false;
    private String wsaFrom;
    private String wsaTo;
    private String wsaAction;
    private String wsaFaultTo;
    private String wsaReplyTo;

    private boolean useWssec = false;
    private String wssecUsername;
    private String wssecPassword;

    private String placeholderPrefix = null;

    private List<WebserviceProperty> properties = new ArrayList<>();

    public Webservice() {}

    public String getPlaceholderPrefix() {
        return placeholderPrefix;
    }

    public void setPlaceholderPrefix(String placeholderPrefix) {
        this.placeholderPrefix = placeholderPrefix;
    }

    @Override
    public String getUuid() {
        return uuid;
    }

    @Override
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public Boolean getUseWsa() {
        return useWsa;
    }

    public void setUseWsa(Boolean useWsa) {
        this.useWsa = useWsa;
    }

    public String getWsaFrom() {
        return wsaFrom;
    }

    public void setWsaFrom(String wsaFrom) {
        this.wsaFrom = wsaFrom;
    }

    public String getWsaTo() {
        return wsaTo;
    }

    public void setWsaTo(String wsaTo) {
        this.wsaTo = wsaTo;
    }

    public String getWsaAction() {
        return wsaAction;
    }

    public void setWsaAction(String wsaAction) {
        this.wsaAction = wsaAction;
    }

    public String getWsaFaultTo() {
        return wsaFaultTo;
    }

    public void setWsaFaultTo(String wsaFaultTo) {
        this.wsaFaultTo = wsaFaultTo;
    }

    public String getWsaReplyTo() {
        return wsaReplyTo;
    }

    public void setWsaReplyTo(String wsaReplyTo) {
        this.wsaReplyTo = wsaReplyTo;
    }

    public boolean getUseWssec() {
        return useWssec;
    }

    public void setUseWssec(boolean useWssec) {
        this.useWssec = useWssec;
    }

    public String getWssecUsername() {
        return wssecUsername;
    }

    public void setWssecUsername(String wssecUsername) {
        this.wssecUsername = wssecUsername;
    }

    public String getWssecPassword() {
        return wssecPassword;
    }

    public void setWssecPassword(String wssecPassword) {
        this.wssecPassword = wssecPassword;
    }

    public List<WebserviceProperty> getProperties() {
        return properties;
    }

    @Override
    public String resolveToken(String tokenName) {
        if (placeholders == null) {
            collectPlaceholders();
        }

        if (placeholders.containsKey(tokenName)) {
            return placeholders.get(tokenName);
        }

        return null;
    }

    public void collectPlaceholders() {
        placeholders = new HashMap<>();
        placeholders.put(WebservicePlaceHolderSuffix.WS_DOMAIN(getPlaceholderPrefix()), getDomain());
        placeholders.put(WebservicePlaceHolderSuffix.WS_CONTEXT(getPlaceholderPrefix()), getContext());
        placeholders.put(WebservicePlaceHolderSuffix.WS_PATH(getPlaceholderPrefix()), getPath());
        placeholders.put(WebservicePlaceHolderSuffix.WS_PORT(getPlaceholderPrefix()), String.valueOf(getPort()));
        placeholders.put(WebservicePlaceHolderSuffix.WS_TIMEOUT(getPlaceholderPrefix()), String.valueOf(getTimeout()));

        if (getUseWsa()) {
            placeholders.put(WebservicePlaceHolderSuffix.WS_WSA_FROM(getPlaceholderPrefix()), getWsaFrom());
            placeholders.put(WebservicePlaceHolderSuffix.WS_WSA_TO(getPlaceholderPrefix()), getWsaTo());
            placeholders.put(WebservicePlaceHolderSuffix.WS_WSA_ACTION(getPlaceholderPrefix()), getWsaAction());
            placeholders.put(WebservicePlaceHolderSuffix.WS_WSA_FAULTTO(getPlaceholderPrefix()), getWsaFaultTo());
            placeholders.put(WebservicePlaceHolderSuffix.WS_WSA_REPLYTO(getPlaceholderPrefix()), getWsaReplyTo());
        }

        if (getUseWssec()) {
            placeholders.put(WebservicePlaceHolderSuffix.WS_WSSEC_USERNAME(getPlaceholderPrefix()), getWssecUsername());
            placeholders.put(WebservicePlaceHolderSuffix.WS_WSSEC_PASSWORD(getPlaceholderPrefix()), getWssecPassword());
        }

        for (WebserviceProperty webserviceProperty : properties) {
            placeholders.put(webserviceProperty.getKey(), webserviceProperty.getValue());
        }
    }

}
