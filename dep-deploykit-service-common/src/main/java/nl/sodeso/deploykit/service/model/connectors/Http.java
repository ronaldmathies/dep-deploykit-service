package nl.sodeso.deploykit.service.model.connectors;

import nl.sodeso.deploykit.service.model.Resource;

/**
 * @author Ronald Mathies
 */
public class Http extends Resource {

    private String label;
    private Integer port = null;
    private Integer timeout = null;
    private String host = null;

    public Http() {}

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
