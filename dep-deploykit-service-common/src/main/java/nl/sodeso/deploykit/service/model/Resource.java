package nl.sodeso.deploykit.service.model;

/**
 * @author Ronald Mathies
 */
public abstract class Resource {

    private String uuid;

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

}
