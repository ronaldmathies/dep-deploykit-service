package nl.sodeso.deploykit.service.builder;

import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.service.model.Profile;

/**
 * @author Ronald Mathies
 */
public class ProfileBuilder {

    public static Profile from(DoProfile doProfile) {
        Profile profile = new Profile();
        profile.setUuid(doProfile.getUuid());
        profile.setLabel(doProfile.getLabel().getValue());
        profile.setVersion(doProfile.getVersion().getValue().getKey());
        profile.setDatasources(DatasourceBuilder.from(doProfile.getDatasources()));
        profile.setCredentials(CredentialBuilder.from(doProfile.getCredentials()));
        profile.setLdapServices(LdapServiceBuilder.from(doProfile.getLdapServices()));
        profile.setWebservices(WebserviceBuilder.from(doProfile.getWebservices()));
        profile.setHttps(HttpsBuilder.from(doProfile.getHttps()));
        profile.setHttp(HttpBuilder.from(doProfile.getHttp()));
        profile.setAccessControl(AccessControlBuilder.from(doProfile.getAccessControl()));
        return profile;
    }

}
