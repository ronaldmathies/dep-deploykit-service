package nl.sodeso.deploykit.service.model.datasource.jdbcdriver;

/**
 * @author Ronald Mathies
 */
public class JdbcDriverJar {

    private String filename;
    private String jar;

    public JdbcDriverJar() {}

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFilename() {
        return this.filename;
    }

    public void setJar(String jar) {
        this.jar = jar;
    }

    public String getJar() {
        return this.jar;
    }

}
