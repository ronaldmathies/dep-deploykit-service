package nl.sodeso.deploykit.service.model.services.webservice;

/**
 * @author Ronald Mathies
 */
public class WebserviceProperty {

    private String key;
    private String value;

    public WebserviceProperty() {}

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return this.key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

}
