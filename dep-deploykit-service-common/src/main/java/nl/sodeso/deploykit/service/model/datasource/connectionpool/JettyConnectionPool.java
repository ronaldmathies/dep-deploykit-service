package nl.sodeso.deploykit.service.model.datasource.connectionpool;

/**
 * @author Ronald Mathies
 */
public class JettyConnectionPool extends ConnectionPool {


    private Integer initialSize = null;
    private Integer maxTotal = null;
    private Integer maxIdle = null;
    private Integer minIdle = null;
    private Integer maxWaitMillis = null;

    private String validationQuery= null;
    private Boolean testOnCreate = null;
    private Boolean testOnBorrow = null;
    private Boolean testOnReturn = null;
    private Boolean testWhileIdle = null;
    private Integer timeBetweenEvictionRunsMillis = null;
    private Integer numTestsPerEvictionRun = null;
    private Integer minEvictableIdleTimeMillis = null;
    private Integer softMiniEvictableIdleTimeMillis = null;
    private Integer maxConnLifetimeMillis = null;
    private Boolean logExpiredConnections = null;
    private String connectionInitSqls= null;
    private Boolean lifo = null;

    private Integer defaultAutoCommit = null;
    private Integer defaultReadOnly = null;
    private String defaultTransactionIsolation = null;
    private String defaultCatalog= null;
    private Boolean cacheState = null;

    public JettyConnectionPool() {}


    public Integer getInitialSize() {
        return initialSize;
    }

    public void setInitialSize(Integer initialSize) {
        this.initialSize = initialSize;
    }

    public Integer getMaxTotal() {
        return maxTotal;
    }

    public void setMaxTotal(Integer maxTotal) {
        this.maxTotal = maxTotal;
    }

    public Integer getMaxIdle() {
        return maxIdle;
    }

    public void setMaxIdle(Integer maxIdle) {
        this.maxIdle = maxIdle;
    }

    public Integer getMinIdle() {
        return minIdle;
    }

    public void setMinIdle(Integer minIdle) {
        this.minIdle = minIdle;
    }

    public Integer getMaxWaitMillis() {
        return maxWaitMillis;
    }

    public void setMaxWaitMillis(Integer maxWaitMillis) {
        this.maxWaitMillis = maxWaitMillis;
    }

    public String getValidationQuery() {
        return validationQuery;
    }

    public void setValidationQuery(String validationQuery) {
        this.validationQuery = validationQuery;
    }

    public Boolean getTestOnCreate() {
        return testOnCreate;
    }

    public void setTestOnCreate(Boolean testOnCreate) {
        this.testOnCreate = testOnCreate;
    }

    public Boolean getTestOnBorrow() {
        return testOnBorrow;
    }

    public void setTestOnBorrow(Boolean testOnBorrow) {
        this.testOnBorrow = testOnBorrow;
    }

    public Boolean getTestOnReturn() {
        return testOnReturn;
    }

    public void setTestOnReturn(Boolean testOnReturn) {
        this.testOnReturn = testOnReturn;
    }

    public Boolean getTestWhileIdle() {
        return testWhileIdle;
    }

    public void setTestWhileIdle(Boolean testWhileIdle) {
        this.testWhileIdle = testWhileIdle;
    }

    public Integer getTimeBetweenEvictionRunsMillis() {
        return timeBetweenEvictionRunsMillis;
    }

    public void setTimeBetweenEvictionRunsMillis(Integer timeBetweenEvictionRunsMillis) {
        this.timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
    }

    public Integer getNumTestsPerEvictionRun() {
        return numTestsPerEvictionRun;
    }

    public void setNumTestsPerEvictionRun(Integer numTestsPerEvictionRun) {
        this.numTestsPerEvictionRun = numTestsPerEvictionRun;
    }

    public Integer getMinEvictableIdleTimeMillis() {
        return minEvictableIdleTimeMillis;
    }

    public void setMinEvictableIdleTimeMillis(Integer minEvictableIdleTimeMillis) {
        this.minEvictableIdleTimeMillis = minEvictableIdleTimeMillis;
    }

    public Integer getSoftMiniEvictableIdleTimeMillis() {
        return softMiniEvictableIdleTimeMillis;
    }

    public void setSoftMiniEvictableIdleTimeMillis(Integer softMiniEvictableIdleTimeMillis) {
        this.softMiniEvictableIdleTimeMillis = softMiniEvictableIdleTimeMillis;
    }

    public Integer getMaxConnLifetimeMillis() {
        return maxConnLifetimeMillis;
    }

    public void setMaxConnLifetimeMillis(Integer maxConnLifetimeMillis) {
        this.maxConnLifetimeMillis = maxConnLifetimeMillis;
    }

    public Boolean getLogExpiredConnections() {
        return logExpiredConnections;
    }

    public void setLogExpiredConnections(Boolean logExpiredConnections) {
        this.logExpiredConnections = logExpiredConnections;
    }

    public String getConnectionInitSqls() {
        return connectionInitSqls;
    }

    public void setConnectionInitSqls(String connectionInitSqls) {
        this.connectionInitSqls = connectionInitSqls;
    }

    public Boolean getLifo() {
        return lifo;
    }

    public void setLifo(Boolean lifo) {
        this.lifo = lifo;
    }

    public Integer getDefaultAutoCommit() {
        return defaultAutoCommit;
    }

    public void setDefaultAutoCommit(Integer defaultAutoCommit) {
        this.defaultAutoCommit = defaultAutoCommit;
    }

    public Integer getDefaultReadOnly() {
        return defaultReadOnly;
    }

    public void setDefaultReadOnly(Integer defaultReadOnly) {
        this.defaultReadOnly = defaultReadOnly;
    }

    public String getDefaultTransactionIsolation() {
        return defaultTransactionIsolation;
    }

    public void setDefaultTransactionIsolation(String defaultTransactionIsolation) {
        this.defaultTransactionIsolation = defaultTransactionIsolation;
    }

    public String getDefaultCatalog() {
        return defaultCatalog;
    }

    public void setDefaultCatalog(String defaultCatalog) {
        this.defaultCatalog = defaultCatalog;
    }

    public Boolean getCacheState() {
        return cacheState;
    }

    public void setCacheState(Boolean cacheState) {
        this.cacheState = cacheState;
    }

}
