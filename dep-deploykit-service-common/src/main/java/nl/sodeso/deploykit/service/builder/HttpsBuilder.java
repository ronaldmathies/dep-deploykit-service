package nl.sodeso.deploykit.service.builder;

import nl.sodeso.deploykit.console.domain.connectors.https.entities.DoHttps;
import nl.sodeso.deploykit.console.domain.connectors.https.entities.DoKeystore;
import nl.sodeso.deploykit.service.builder.util.Base64Util;
import nl.sodeso.deploykit.service.model.connectors.Https;
/**
 * @author Ronald Mathies
 */
public class HttpsBuilder {

    public static Https from(DoHttps doHttps) {
        if (doHttps != null) {

            Https https = new Https();
            https.setUuid(doHttps.getUuid());
            https.setLabel(doHttps.getLabel().getValue());
            https.setKeyStorePassword(doHttps.getKeyStorePassword().getPassword().getValue());
            https.setTrustStorePassword(doHttps.getTrustStorePassword().getPassword().getValue());
            https.setKeyManagerPassword(doHttps.getKeyManagerPassword().getPassword().getValue());
            https.setPort(doHttps.getPort().getValue());
            https.setTimeout(doHttps.getTimeout().getValue());
            https.setHost(doHttps.getHost().getValue());

            DoKeystore keystore = doHttps.getKeystore();
            https.setKeystore(Base64Util.to(keystore.getKeystore()).toString());
            https.setFilename(keystore.getFilename());

            return https;
        }

        return null;
    }

}
