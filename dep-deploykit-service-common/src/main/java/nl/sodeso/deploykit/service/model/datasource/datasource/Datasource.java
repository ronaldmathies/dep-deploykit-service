package nl.sodeso.deploykit.service.model.datasource.datasource;

import nl.sodeso.commons.token.ITokenResolver;
import nl.sodeso.deploykit.console.client.application.datasources.datasource.DatasourcePlaceHolderSuffix;
import nl.sodeso.deploykit.service.model.Resource;
import nl.sodeso.deploykit.service.model.datasource.connectionpool.JBossConnectionPool;
import nl.sodeso.deploykit.service.model.datasource.connectionpool.JettyConnectionPool;
import nl.sodeso.deploykit.service.model.datasource.jdbcdriver.JdbcDriver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class Datasource extends Resource implements ITokenResolver {

    private Map<String, String> placeholders = null;

    private String label = null;
    private String placeholderPrefix = null;
    private String jndi = null;
    private String url = null;
    private String username = null;
    private String password = null;

    private JettyConnectionPool jettyConnectionPools = null;
    private JBossConnectionPool jBossConnectionPools = null;

    private JdbcDriver jdbcDriver = null;

    private List<ConnectionProperty> connectionProperties = new ArrayList<>();

    public Datasource() {}

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPlaceholderPrefix() {
        return placeholderPrefix;
    }

    public void setPlaceholderPrefix(String placeholderPrefix) {
        this.placeholderPrefix = placeholderPrefix;
    }

    public String getJndi() {
        return jndi;
    }

    public void setJndi(String jndi) {
        this.jndi = jndi;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public JettyConnectionPool getJettyConnectionPool() {
        return jettyConnectionPools;
    }

    public void setJettyConnectionPool(JettyConnectionPool jettyConnectionPools) {
        this.jettyConnectionPools = jettyConnectionPools;
    }

    public JBossConnectionPool getJBossConnectionPool() {
        return jBossConnectionPools;
    }

    public void setJBossConnectionPool(JBossConnectionPool jBossConnectionPools) {
        this.jBossConnectionPools = jBossConnectionPools;
    }

    public void setJdbcDriver(JdbcDriver jdbcDriver) {
        this.jdbcDriver = jdbcDriver;
    }

    public JdbcDriver getJdbcDriver() {
        return this.jdbcDriver;
    }

    public List<ConnectionProperty> getConnectionProperties() {
        return this.connectionProperties;
    }

    @Override
    public String resolveToken(String tokenName) {
        if (placeholders == null) {
            collectPlaceholders();
        }

        if (placeholders.containsKey(tokenName)) {
            return placeholders.get(tokenName);
        }

        return null;
    }

    public void collectPlaceholders() {
        placeholders = new HashMap<>();
        placeholders.put(DatasourcePlaceHolderSuffix.JDBC_JNDI(getPlaceholderPrefix()), getJndi());
        placeholders.put(DatasourcePlaceHolderSuffix.JDBC_URL(getPlaceholderPrefix()), getUrl());
        placeholders.put(DatasourcePlaceHolderSuffix.JDBC_DRIVER_CLASS(getPlaceholderPrefix()), getJdbcDriver().getJdbcDriverClass());
        placeholders.put(DatasourcePlaceHolderSuffix.JDBC_USERNAME(getPlaceholderPrefix()), getUsername());
        placeholders.put(DatasourcePlaceHolderSuffix.JDBC_PASSWORD(getPlaceholderPrefix()), getPassword());

        for (ConnectionProperty connectionProperty : connectionProperties) {
            placeholders.put(connectionProperty.getKey(), connectionProperty.getValue());
        }
    }


}
