package nl.sodeso.deploykit.service.model.accesscontrol;

import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.AccessControlPlaceHolderSuffix;
import nl.sodeso.deploykit.service.model.datasource.datasource.Datasource;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class DatasourceAccessControl extends AccessControl {

    private Map<String, String> placeholders = null;

    private String userTable;
    private String userColumn;
    private String credentialColumn;
    private String roleTable;
    private String roleUserColumn;
    private String roleRoleColumn;

    private Datasource datasource;

    public DatasourceAccessControl() {}

    public String getUserTable() {
        return userTable;
    }

    public void setUserTable(String userTable) {
        this.userTable = userTable;
    }

    public String getUserColumn() {
        return userColumn;
    }

    public void setUserColumn(String userColumn) {
        this.userColumn = userColumn;
    }

    public String getCredentialColumn() {
        return credentialColumn;
    }

    public void setCredentialColumn(String credentialColumn) {
        this.credentialColumn = credentialColumn;
    }

    public String getRoleTable() {
        return roleTable;
    }

    public void setRoleTable(String roleTable) {
        this.roleTable = roleTable;
    }

    public String getRoleUserColumn() {
        return roleUserColumn;
    }

    public void setRoleUserColumn(String roleUserColumn) {
        this.roleUserColumn = roleUserColumn;
    }

    public String getRoleRoleColumn() {
        return roleRoleColumn;
    }

    public void setRoleRoleColumn(String roleRoleColumn) {
        this.roleRoleColumn = roleRoleColumn;
    }

    public Datasource getDatasource() {
        return datasource;
    }

    public void setDatasource(Datasource datasource) {
        this.datasource = datasource;
    }

    @Override
    public String resolveToken(String tokenName) {
        if (placeholders == null) {
            collectPlaceholders();
        }

        if (placeholders.containsKey(tokenName)) {
            return placeholders.get(tokenName);
        }

        return null;
    }

    public void collectPlaceholders() {
        placeholders = new HashMap<>();

        placeholders.put(AccessControlPlaceHolderSuffix.AC_REALM(getPlaceholderPrefix()), getRealm());
        placeholders.put(AccessControlPlaceHolderSuffix.AC_DS_USER_TABLE(getPlaceholderPrefix()), getUserTable());
        placeholders.put(AccessControlPlaceHolderSuffix.AC_DS_USER_USER_COLUMN(getPlaceholderPrefix()), getUserColumn());
        placeholders.put(AccessControlPlaceHolderSuffix.AC_DS_USER_CREDENTIAL_COLUMN(getPlaceholderPrefix()), getCredentialColumn());
        placeholders.put(AccessControlPlaceHolderSuffix.AC_DS_ROLE_TABLE(getPlaceholderPrefix()), getRoleTable());
        placeholders.put(AccessControlPlaceHolderSuffix.AC_DS_ROLE_USER_COLUMN(getPlaceholderPrefix()), getRoleUserColumn());
        placeholders.put(AccessControlPlaceHolderSuffix.AC_DS_ROLE_ROLE_COLUMN(getPlaceholderPrefix()), getRoleRoleColumn());

    }
}
