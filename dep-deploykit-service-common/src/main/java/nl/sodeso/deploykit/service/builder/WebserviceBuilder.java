package nl.sodeso.deploykit.service.builder;

import nl.sodeso.deploykit.console.domain.services.webservice.entities.DoWebservice;
import nl.sodeso.deploykit.console.domain.services.webservice.entities.DoWebserviceProperty;
import nl.sodeso.deploykit.service.model.services.webservice.Webservice;
import nl.sodeso.deploykit.service.model.services.webservice.WebserviceProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class WebserviceBuilder {

    public static List<Webservice> from(List<DoWebservice> doWebservices) {
        List<Webservice> webservices = new ArrayList<>();
        for (DoWebservice doWebservice : doWebservices) {
            webservices.add(from(doWebservice));
        }
        return webservices;
    }

    public static Webservice from(DoWebservice doWebservice) {
        Webservice webservice = new Webservice();
        webservice.setUuid(doWebservice.getUuid());
        webservice.setPlaceholderPrefix(doWebservice.getPlaceholderPrefix().getValue());
        webservice.setLabel(doWebservice.getLabel().getValue());
        webservice.setDomain(doWebservice.getDomain().getValue());
        webservice.setPort(doWebservice.getPort().getValue());
        webservice.setPath(doWebservice.getPath().getValue());
        webservice.setContext(doWebservice.getContext().getValue());
        webservice.setTimeout(doWebservice.getTimeout().getOriginalValue());

        if (doWebservice.getUseWsAddressing().getValue()) {
            webservice.setUseWsa(true);
            webservice.setWsaFrom(doWebservice.getFrom().getValue());
            webservice.setWsaTo(doWebservice.getTo().getValue());
            webservice.setWsaAction(doWebservice.getAction().getValue());
            webservice.setWsaFaultTo(doWebservice.getFaultTo().getValue());
            webservice.setWsaReplyTo(doWebservice.getReplyTo().getValue());
        }

        if (doWebservice.getCredential() != null) {
            webservice.setUseWssec(true);
            webservice.setWssecUsername(doWebservice.getCredential().getUsername().getValue());
            webservice.setWssecPassword(doWebservice.getCredential().getPassword().getValue());
        }

        for (DoWebserviceProperty doWebserviceProperty : doWebservice.getProperties()) {
            WebserviceProperty webserviceProperty = new WebserviceProperty();
            webserviceProperty.setKey(doWebserviceProperty.getKey().getValue());
            webserviceProperty.setValue(doWebserviceProperty.getValue().getValue());
            webservice.getProperties().add(webserviceProperty);
        }

        return webservice;
    }

}
