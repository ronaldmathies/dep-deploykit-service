package nl.sodeso.deploykit.service.model.general.credential;

import nl.sodeso.commons.token.ITokenResolver;
import nl.sodeso.deploykit.console.client.application.general.credential.CredentialPlaceHolderSuffix;
import nl.sodeso.deploykit.service.model.Resource;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class Credential extends Resource implements ITokenResolver {

    private Map<String, String> placeholders = null;

    private String label = null;
    private String placeholderPrefix = null;

    private String username;
    private String password;

    public Credential() {}

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPlaceholderPrefix() {
        return placeholderPrefix;
    }

    public void setPlaceholderPrefix(String placeholderPrefix) {
        this.placeholderPrefix = placeholderPrefix;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String resolveToken(String tokenName) {
        if (placeholders == null) {
            collectPlaceholders();
        }

        if (placeholders.containsKey(tokenName)) {
            return placeholders.get(tokenName);
        }

        return null;
    }

    public void collectPlaceholders() {
        placeholders = new HashMap<>();
        placeholders.put(CredentialPlaceHolderSuffix.USERNAME(getPlaceholderPrefix()), getUsername());
        placeholders.put(CredentialPlaceHolderSuffix.PASSWORD(getPlaceholderPrefix()), getPassword());
    }
}
