package nl.sodeso.deploykit.service.model.accesscontrol;

import nl.sodeso.commons.token.ITokenResolver;
import nl.sodeso.deploykit.service.model.Resource;

/**
 * @author Ronald Mathies
 */
public abstract class AccessControl extends Resource implements ITokenResolver {

    private String placeholderPrefix = null;
    private String realm;
    private String label;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getRealm() {
        return realm;
    }

    public void setRealm(String realm) {
        this.realm = realm;
    }

    public String getPlaceholderPrefix() {
        return placeholderPrefix;
    }

    public void setPlaceholderPrefix(String placeholderPrefix) {
        this.placeholderPrefix = placeholderPrefix;
    }

}
