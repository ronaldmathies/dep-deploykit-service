package nl.sodeso.deploykit.service.builder;

import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPoolJBoss;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPoolJetty;
import nl.sodeso.deploykit.service.model.datasource.connectionpool.JBossConnectionPool;
import nl.sodeso.deploykit.service.model.datasource.connectionpool.JettyConnectionPool;

/**
 * @author Ronald Mathies
 */
public class ConnectionPoolBuilder {

    public static JettyConnectionPool from(DoConnectionPoolJetty doConnectionPool) {
        JettyConnectionPool connectionPool = new JettyConnectionPool();
        connectionPool.setInitialSize(doConnectionPool.getInitialSize().getValue());
        connectionPool.setMaxTotal(doConnectionPool.getMaxTotal().getValue());
        connectionPool.setMaxIdle(doConnectionPool.getMaxIdle().getValue());
        connectionPool.setMinIdle(doConnectionPool.getMinIdle().getValue());
        connectionPool.setMaxWaitMillis(doConnectionPool.getMaxWaitMillis().getValue());
        connectionPool.setValidationQuery(doConnectionPool.getValidationQuery().getValue());
        connectionPool.setTestOnCreate(doConnectionPool.getTestOnCreate().getValue());
        connectionPool.setTestOnBorrow(doConnectionPool.getTestOnBorrow().getValue());
        connectionPool.setTestOnReturn(doConnectionPool.getTestOnReturn().getValue());
        connectionPool.setTestWhileIdle(doConnectionPool.getTestWhileIdle().getValue());
        connectionPool.setTimeBetweenEvictionRunsMillis(doConnectionPool.getTimeBetweenEvictionRunsMillis().getValue());
        connectionPool.setNumTestsPerEvictionRun(doConnectionPool.getNumTestsPerEvictionRun().getValue());
        connectionPool.setMinEvictableIdleTimeMillis(doConnectionPool.getMinEvictableIdleTimeMillis().getValue());
        connectionPool.setSoftMiniEvictableIdleTimeMillis(doConnectionPool.getSoftMiniEvictableIdleTimeMillis().getValue());
        connectionPool.setMaxConnLifetimeMillis(doConnectionPool.getMaxConnLifetimeMillis().getValue());
        connectionPool.setLogExpiredConnections(doConnectionPool.getLogExpiredConnections().getValue());
        connectionPool.setConnectionInitSqls(doConnectionPool.getConnectionInitSqls().getValue());
        connectionPool.setLifo(doConnectionPool.getLifo().getValue());
        connectionPool.setDefaultAutoCommit(doConnectionPool.getDefaultAutoCommit().getValue());
        connectionPool.setDefaultReadOnly(doConnectionPool.getDefaultReadOnly().getValue());
        if ("DRIVER_DEFAULT".equalsIgnoreCase(doConnectionPool.getDefaultTransactionIsolation().getValue().getDescription())) {
            connectionPool.setDefaultTransactionIsolation(doConnectionPool.getDefaultTransactionIsolation().getValue().getDescription());
        }
        connectionPool.setDefaultCatalog(doConnectionPool.getDefaultCatalog().getValue());
        connectionPool.setCacheState(doConnectionPool.getCacheState().getValue());

        return connectionPool;
    }

    public static JBossConnectionPool from(DoConnectionPoolJBoss doConnectionPool) {
        JBossConnectionPool connectionPool = new JBossConnectionPool();

        connectionPool.setMinPoolSize(doConnectionPool.getMinPoolSize().getValue());
        connectionPool.setMaxPoolSize(doConnectionPool.getMaxPoolSize().getValue());
        connectionPool.setInitialPoolSize(doConnectionPool.getInitialPoolSize().getValue());
        connectionPool.setPrefill(doConnectionPool.getPrefill().getValue());
        connectionPool.setAllocationRetry(doConnectionPool.getAllocationRetry().getValue());
        connectionPool.setAllocationRetryWaitMillis(doConnectionPool.getAllocationRetryWaitMillis().getValue());
        connectionPool.setBlockingTimeoutMillis(doConnectionPool.getBlockingTimeoutMillis().getValue());
        connectionPool.setIdleTimeoutMinutes(doConnectionPool.getIdleTimeoutMinutes().getValue());
        connectionPool.setConnectionInitSqls(doConnectionPool.getConnectionInitSqls().getValue());
        connectionPool.setValidationQuery(doConnectionPool.getValidationQuery().getValue());
        if (!"DRIVER_DEFAULT".equalsIgnoreCase(doConnectionPool.getDefaultTransactionIsolation().getValue().getDescription())) {
            connectionPool.setDefaultTransactionIsolation(doConnectionPool.getDefaultTransactionIsolation().getValue().getDescription());
        }
        return connectionPool;
    }

}
