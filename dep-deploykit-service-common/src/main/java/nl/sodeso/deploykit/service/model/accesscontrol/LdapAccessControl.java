package nl.sodeso.deploykit.service.model.accesscontrol;

import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.AccessControlPlaceHolderSuffix;
import nl.sodeso.deploykit.service.model.services.ldap.LdapService;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class LdapAccessControl extends AccessControl {

    private Map<String, String> placeholders = null;

    private String userBaseDn;
    private String userAttribute;
    private String roleBaseDn;
    private String roleMemberAttribute;
    private String roleNameAttribute;
    private Boolean forceBindingLogin;
    private String userPasswordAttribute;
    private String userObjectClass;
    private String roleObjectClass;

    private LdapService ldapService;

    public LdapAccessControl() {}

    public String getUserBaseDn() {
        return userBaseDn;
    }

    public void setUserBaseDn(String userBaseDn) {
        this.userBaseDn = userBaseDn;
    }

    public String getUserAttribute() {
        return userAttribute;
    }

    public void setUserAttribute(String userAttribute) {
        this.userAttribute = userAttribute;
    }

    public String getRoleBaseDn() {
        return roleBaseDn;
    }

    public void setRoleBaseDn(String roleBaseDn) {
        this.roleBaseDn = roleBaseDn;
    }

    public String getRoleMemberAttribute() {
        return roleMemberAttribute;
    }

    public void setRoleMemberAttribute(String roleMemberAttribute) {
        this.roleMemberAttribute = roleMemberAttribute;
    }

    public String getRoleNameAttribute() {
        return roleNameAttribute;
    }

    public void setRoleNameAttribute(String roleNameAttribute) {
        this.roleNameAttribute = roleNameAttribute;
    }

    public Boolean getForceBindingLogin() {
        return forceBindingLogin;
    }

    public void setForceBindingLogin(Boolean forceBindingLogin) {
        this.forceBindingLogin = forceBindingLogin;
    }

    public String getUserPasswordAttribute() {
        return userPasswordAttribute;
    }

    public void setUserPasswordAttribute(String userPasswordAttribute) {
        this.userPasswordAttribute = userPasswordAttribute;
    }

    public String getUserObjectClass() {
        return userObjectClass;
    }

    public void setUserObjectClass(String userObjectClass) {
        this.userObjectClass = userObjectClass;
    }

    public String getRoleObjectClass() {
        return roleObjectClass;
    }

    public void setRoleObjectClass(String roleObjectClass) {
        this.roleObjectClass = roleObjectClass;
    }

    public LdapService getLdapService() {
        return ldapService;
    }

    public void setLdapService(LdapService ldapService) {
        this.ldapService = ldapService;
    }

    @Override
    public String resolveToken(String tokenName) {
        if (placeholders == null) {
            collectPlaceholders();
        }

        if (placeholders.containsKey(tokenName)) {
            return placeholders.get(tokenName);
        }

        return null;
    }

    public void collectPlaceholders() {
        placeholders = new HashMap<>();
        placeholders.put(AccessControlPlaceHolderSuffix.AC_REALM(getPlaceholderPrefix()), getRealm());
        placeholders.put(AccessControlPlaceHolderSuffix.AC_LDAP_USER_BASEDN(getPlaceholderPrefix()), getUserBaseDn());
        placeholders.put(AccessControlPlaceHolderSuffix.AC_LDAP_USER_ATTRIBUTE(getPlaceholderPrefix()), getUserAttribute());
        placeholders.put(AccessControlPlaceHolderSuffix.AC_LDAP_USER_PASSWORD_ATTRIBUTE(getPlaceholderPrefix()), getUserPasswordAttribute());
        placeholders.put(AccessControlPlaceHolderSuffix.AC_LDAP_USER_OBJECTCLASS(getPlaceholderPrefix()), getUserObjectClass());
        placeholders.put(AccessControlPlaceHolderSuffix.AC_LDAP_ROLE_BASEDN(getPlaceholderPrefix()), getRoleBaseDn());
        placeholders.put(AccessControlPlaceHolderSuffix.AC_LDAP_ROLE_MEMBER_ATTRIBUTE(getPlaceholderPrefix()), getRoleMemberAttribute());
        placeholders.put(AccessControlPlaceHolderSuffix.AC_LDAP_ROLE_NAME_ATTRIBUTE(getPlaceholderPrefix()), getRoleNameAttribute());
        placeholders.put(AccessControlPlaceHolderSuffix.AC_LDAP_ROLE_OBJECTCLASS(getPlaceholderPrefix()), getRoleObjectClass());
    }
}
