package nl.sodeso.deploykit.service.builder;

import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControl;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControlDatasource;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControlLdap;
import nl.sodeso.deploykit.service.model.accesscontrol.AccessControl;
import nl.sodeso.deploykit.service.model.accesscontrol.DatasourceAccessControl;
import nl.sodeso.deploykit.service.model.accesscontrol.LdapAccessControl;

/**
 * @author Ronald Mathies
 */
public class AccessControlBuilder {

    public static AccessControl from(DoAccessControl doAccessControl) {
        if (doAccessControl instanceof DoAccessControlLdap) {
            return from((DoAccessControlLdap)doAccessControl);
        }

        return from((DoAccessControlDatasource)doAccessControl);
    }

    public static LdapAccessControl from(DoAccessControlLdap doAccessControlLdap) {
        LdapAccessControl ldapAccessControl = new LdapAccessControl();

        ldapAccessControl.setUuid(doAccessControlLdap.getUuid());
        ldapAccessControl.setLabel(doAccessControlLdap.getLabel().getValue());
        ldapAccessControl.setRealm(doAccessControlLdap.getRealm().getValue());
        ldapAccessControl.setPlaceholderPrefix(doAccessControlLdap.getPlaceholderPrefix().getValue());

        ldapAccessControl.setUserBaseDn(doAccessControlLdap.getUserBaseDn().getValue());
        ldapAccessControl.setUserAttribute(doAccessControlLdap.getUserAttribute().getValue());
        ldapAccessControl.setUserPasswordAttribute(doAccessControlLdap.getUserPasswordAttribute().getValue());
        ldapAccessControl.setUserObjectClass(doAccessControlLdap.getUserObjectClass().getValue());
        ldapAccessControl.setRoleBaseDn(doAccessControlLdap.getRoleBaseDn().getValue());
        ldapAccessControl.setRoleNameAttribute(doAccessControlLdap.getRoleNameAttribute().getValue());
        ldapAccessControl.setRoleMemberAttribute(doAccessControlLdap.getRoleMemberAttribute().getValue());
        ldapAccessControl.setRoleObjectClass(doAccessControlLdap.getRoleObjectClass().getValue());
        ldapAccessControl.setLdapService(LdapServiceBuilder.from(doAccessControlLdap.getLdapService()));
        ldapAccessControl.setForceBindingLogin(doAccessControlLdap.getForceBindingLogin().getValue());

        return ldapAccessControl;
    }

    public static DatasourceAccessControl from (DoAccessControlDatasource doAccessControlDatasource) {
        DatasourceAccessControl datasourceAccessControl = new DatasourceAccessControl();

        datasourceAccessControl.setUuid(doAccessControlDatasource.getUuid());
        datasourceAccessControl.setLabel(doAccessControlDatasource.getLabel().getValue());
        datasourceAccessControl.setRealm(doAccessControlDatasource.getRealm().getValue());
        datasourceAccessControl.setPlaceholderPrefix(doAccessControlDatasource.getPlaceholderPrefix().getValue());
        datasourceAccessControl.setUserTable(doAccessControlDatasource.getUserTable().getValue());
        datasourceAccessControl.setUserColumn(doAccessControlDatasource.getUserColumn().getValue());
        datasourceAccessControl.setRoleTable(doAccessControlDatasource.getRoleTable().getValue());
        datasourceAccessControl.setRoleUserColumn(doAccessControlDatasource.getRoleUserColumn().getValue());
        datasourceAccessControl.setRoleRoleColumn(doAccessControlDatasource.getRoleRoleColumn().getValue());

        return datasourceAccessControl;
    }

}
