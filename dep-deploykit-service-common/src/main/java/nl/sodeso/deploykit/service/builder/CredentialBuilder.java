package nl.sodeso.deploykit.service.builder;

import nl.sodeso.deploykit.console.domain.general.credential.entities.DoCredential;
import nl.sodeso.deploykit.service.model.general.credential.Credential;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class CredentialBuilder {

    public static List<Credential> from(List<DoCredential> doCredentials) {
        List<Credential> credentials = new ArrayList<>();

        for (DoCredential doCredential : doCredentials) {
            credentials.add(from(doCredential));
        }

        return credentials;
    }

    public static Credential from(DoCredential doCredential) {
        Credential credential = new Credential();

        credential.setUuid(doCredential.getUuid());
        credential.setLabel(doCredential.getLabel().getValue());
        credential.setPlaceholderPrefix(doCredential.getPlaceholderPrefix().getValue());
        credential.setUsername(doCredential.getUsername().getValue());
        credential.setPassword(doCredential.getPassword().getValue());

        return credential;
    }
}
