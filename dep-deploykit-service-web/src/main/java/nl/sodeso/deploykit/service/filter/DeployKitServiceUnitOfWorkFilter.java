package nl.sodeso.deploykit.service.filter;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.persistence.hibernate.filter.UnitOfWorkFilter;

import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

/**
 * @author Ronald Mathies
 */
@WebFilter(
    urlPatterns = {"/*"},
    initParams = {
        @WebInitParam(name = UnitOfWorkFilter.INIT_PERSISTENCE_UNIT, value = Constants.PU)
    }
)
public class DeployKitServiceUnitOfWorkFilter extends UnitOfWorkFilter {
}