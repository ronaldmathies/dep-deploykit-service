package nl.sodeso.deploykit.service.endpoint;

import nl.sodeso.deploykit.service.endpoint.validation.ValidationConfigurationContextResolver;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

/**
 * @author Ronald MAthies
 */
public class DeployKitServiceConfig extends ResourceConfig {

    public DeployKitServiceConfig() {
        packages(DeployKitService.class.getPackage().getName());
        register(JacksonFeature.class);
        register(ValidationConfigurationContextResolver.class);
        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);

    }

}
