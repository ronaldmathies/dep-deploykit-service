package nl.sodeso.deploykit.service.filter;

import nl.sodeso.commons.properties.annotations.FileResource;
import nl.sodeso.commons.properties.annotations.FileResources;
import nl.sodeso.commons.properties.annotations.Resource;
import nl.sodeso.commons.properties.containers.file.FileContainer;
import nl.sodeso.deploykit.console.domain.Constants;

/**
 * @author Ronald Mathies
 */
@Resource(
        domain = Constants.PU
)
@FileResources(
        files = {
                @FileResource(file = "/dep-deploykit-service-persistence.properties")
        }
)
public class UnitOfWorkProperties extends FileContainer {
    
}
