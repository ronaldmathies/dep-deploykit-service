package nl.sodeso.deploykit.service.endpoint.validation;

import nl.sodeso.commons.restful.validation.AbstractValidationConfigurationContextResolver;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ValidationConfigurationContextResolver extends AbstractValidationConfigurationContextResolver {

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getParameterNames(final Constructor<?> constructor) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getParameterNames(final Method method) {
        if ("profile".equals(method.getName())) {
            return Arrays.asList("label", "version");
        }

        return null;
    }
}