package nl.sodeso.deploykit.service.endpoint;

import nl.sodeso.commons.restful.validation.mandatory.MandatoryConstraint;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionType;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.console.domain.profile.ProfileRepository;
import nl.sodeso.deploykit.service.builder.ProfileBuilder;
import nl.sodeso.commons.restful.model.Message;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * @author Ronald Mathies
 */
@Path("/deploykit")
public class DeployKitService {

    private static final String ENCODING = "UTF-8";

    private static final Log LOG = LogFactory.getLog(DeployKitService.class);

    @Context
    private UriInfo headers;

    public DeployKitService() {
    }

    /**
     * Endpoint method that will return the details of the requested profile.
     *
     * @param label the label of the profile.
     * @param version the version of the profile.
     *
     * @return returns the profile details.
     */
    @GET
    @Path("profile")
    @Produces(MediaType.APPLICATION_JSON)
    public Response profile(
            final @MandatoryConstraint(code = "ERR01", message = "Label is mandatory") @QueryParam("label") String label,
            final @MandatoryConstraint(code = "ERR02", message = "Version is mandatory") @QueryParam("version") String version) {
        LOG.info("Start DeployKitService.profile for query param label '" + label + "', version '" + version + "'.");
        Response.ResponseBuilder response = Response.ok();
        response.encoding(ENCODING);

        VersionOption versionOption = createVersionFromArgument(version);
        DoProfile doProfile = new ProfileRepository().findByLabelAndVersion(label, versionOption);
        if (doProfile != null) {
            return response.entity(ProfileBuilder.from(doProfile)).build();
        }


        return Response.status(Response.Status.BAD_REQUEST).entity(new Message("INF-001", "No profile found with label '" + label + "' and version '" + version + "'.")).type(MediaType.APPLICATION_JSON_TYPE).build();
    }

    /**
     * Recreates the version option based on the version string.
     * @param version the version string as per caller.
     * @return the constructed version option.
     */
    public VersionOption createVersionFromArgument(String version) {

        if (version != null && version.length() > 0) {
            if ("SNAPSHOT".equals(version)) {
                return new VersionOption("SNAPSHOT", VersionType.SNAPSHOT);
            } else if (version.contains("BRANCH")) {
                version = version.substring(0, version.indexOf("BRANCH"));
                return new VersionOption(version, VersionType.BRANCH);
            } else {
                return new VersionOption(version, VersionType.RELEASE);
            }
        }

        return null;
    }
}
